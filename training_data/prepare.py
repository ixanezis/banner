#!/usr/bin/env python

from lxml import etree
import sys


def getrange(annotation):
    return (int(annotation.find("location").get("offset")), int(annotation.find("location").get("length")))


if __name__ == "__main__":
    collection = etree.Element('collection')
    for _, element in etree.iterparse(sys.stdin, tag="document", encoding="utf-8", resolve_entities=False, no_network=True):
        collection.append(element)
        for passage in element.iter("passage"):
            annotations = []
            for annotation in passage.iter("annotation"):
                annotations.append(annotation)
                passage.remove(annotation)

            annotations.sort(key=getrange)

            last = min(len(annotations), 1)
            if annotations:
                annotations[0].set("frequency", "1")
            for i in xrange(1, len(annotations)):
                if getrange(annotations[i]) != getrange(annotations[i-1]):
                    annotations[last] = annotations[i]
                    annotations[last].set("frequency", "1")
                    last += 1
                else:
                    annotations[last-1].set("frequency", str(int(annotations[last-1].get("frequency")) + 1))

            print >>sys.stderr, "last =", last
            for i in xrange(last):
                if int(annotations[i].get("frequency")) <= 8:
                    continue
                #if not annotations[i].get("text"):
                    #continue
                #if len(annotations[i].find("text").text) > 31:
                    #continue
                passage.append(annotations[i])
        #element.clear()

    print etree.tostring(collection, pretty_print=True)
