#!/usr/bin/env python

from lxml import etree
import sys
from itertools import izip
from prepare import getrange


if __name__ == "__main__":
    experts = dict()
    with open("fixed_expert.xml") as experts_file:
        for _, element in etree.iterparse(experts_file, tag="document", encoding="utf-8", resolve_entities=False, no_network=True):
            experts[element.find("id").text] = element

    print len(experts)
    with open("prepared_mturk1.xml") as mturk1_file:
        for _, element in etree.iterparse(mturk1_file, tag="document", encoding="utf-8", resolve_entities=False, no_network=True):
            id = element.find("id").text
            if id in experts:
                for passage_expert, passage_mturk in izip(experts[id].iter("passage"), element.iter("passage")):
                    for ann_mturk in passage_mturk.iter("annotation"):
                        #if int(ann_mturk.get("frequency")) <= 7:
                            #continue
                        #if len(ann_mturk.find("text").text) > 37:
                            #continue
                        found = False
                        for ann_expert in passage_expert.iter("annotation"):
                            if getrange(ann_expert) == getrange(ann_mturk):
                                found = True
                                break

                        if found:
                            print "found: '{}' with frequency {} (annotation {})".format(ann_mturk.find("text").text, ann_mturk.get("frequency"), ann_mturk.get("id"))
                        else:
                            print "not found: '{}' with frequency {} (annotation {})".format(ann_mturk.find("text").text, ann_mturk.get("frequency"), ann_mturk.get("id"))
