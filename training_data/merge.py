#!/usr/bin/env python

from lxml import etree
import sys
from itertools import izip


if __name__ == "__main__":
    collection = etree.Element('collection')
    with open(sys.argv[1]) as file:
        for _, document in etree.iterparse(file, tag="document", encoding="utf-8", resolve_entities=False, no_network=True):
            collection.append(document)

    with open(sys.argv[2]) as file:
        for _, document in etree.iterparse(file, tag="document", encoding="utf-8", resolve_entities=False, no_network=True):
            collection.append(document)

    print etree.tostring(collection, pretty_print=True)
